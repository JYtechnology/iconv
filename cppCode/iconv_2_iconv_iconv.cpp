#include "..\include_iconv_header.h"

// 调用格式: SDT_BIN 编码转换_转换, 命令说明: "对指定数据进行编码转换，返回转换后的数据。对同一个“转换句柄”，可多次执行本命令，直到该句柄被关闭。如果执行失败，将返回空字节集，同时设置参数“执行结果”为“假”。"
// 参数<1>: 转换句柄 SDT_INT, 参数说明: "必须是“编码转换_打开()”命令所返回的句柄。"
// 参数<2>: 被转换数据 SDT_BIN, 参数说明: NULL
// 参数<3>: [&执行结果 SDT_BOOL], 参数说明: "如果提供本参数，其中将被写入本命令的执行结果——执行成功时为真，执行失败时为假。"
ICONV_EXTERN_C void Kiiconv_kiiconv_iconv_2_Kiiconv(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	//获取字节集长度和数据指针
	size_t dwDataLen = 0;//待转换数据长度
	LPBYTE pData = 0;//待转换的数据
	DATA_TYPE dtDataType = pArgInf[1].m_dtDataType&~DT_IS_VAR;//数据类型
	bool dtIsVar = (pArgInf[1].m_dtDataType & DT_IS_VAR) != 0;//是否为变量地址
	switch (dtDataType)
	{
	case SDT_BIN:
		//字节集数据
		if (dtIsVar) {
			pData = GetAryElementInf(*pArgInf[1].m_ppBin, (LPINT)&dwDataLen);//待转换数据指针
		}
		else {
			pData = GetAryElementInf(pArgInf[1].m_pBin, (LPINT)&dwDataLen);//待转换数据指针
		}
		
		if (dwDataLen == 0) {//数据长度为0，直接返回
			if (pArgInf[2].m_pBool) *pArgInf[2].m_pBool = false;
			return;
		}
		break;
	case SDT_TEXT:
		//文本数据
		if (dtIsVar) {
			pData = (LPBYTE)*pArgInf[1].m_ppText;
			dwDataLen = strlen(*pArgInf[1].m_ppText);
		}
		else {
			pData = (LPBYTE)pArgInf[1].m_pText;
			dwDataLen = strlen(pArgInf[1].m_pText);
		}
		break;
	default: 
		if (pArgInf[2].m_pBool) *pArgInf[2].m_pBool = false;
		return;
	}

	iconv_t cd = (iconv_t)pArgInf[0].m_int;
	size_t dwBuffLen = dwDataLen * 4;//缓冲区长度
	LPBYTE pBuff = (LPBYTE)ealloc(sizeof(INT) * 2 +dwBuffLen);
	char** outbuf = (char**)(pBuff + sizeof(INT) * 2);
	size_t outbytesleft = dwBuffLen;

	//后面4个参数都会改变，当执行一次后，pData 会移动到 pData+dwDataLen 的位置，dwDataLen=0 ，pBuff 也会移动到新的位置，dwBuffLen 返回 pBuff 还剩下多少
	size_t ret = iconv(cd, (char**)&pData, &dwDataLen, (char**)&outbuf, &outbytesleft);
	if (ret == -1) {
		if (pArgInf[2].m_pBool) *pArgInf[2].m_pBool = false;
		efree(pBuff);
		return;
	}

	*(LPINT)pBuff = 1;
	*(LPINT)(pBuff + sizeof(INT)) = dwBuffLen - outbytesleft;
	pRetData->m_pBin = pBuff;
	if (pArgInf[2].m_pBool) *pArgInf[2].m_pBool = true;
}


// 调用格式: SDT_BIN 编码转换_转换, 命令说明: "对指定数据进行编码转换，返回转换后的数据。对同一个“转换句柄”，可多次执行本命令，直到该句柄被关闭。如果执行失败，将返回空字节集，同时设置参数“执行结果”为“假”。"
// 参数<1>: 转换句柄 SDT_INT, 参数说明: "必须是“编码转换_打开()”命令所返回的句柄。"
// 参数<2>: 被转换数据 SDT_BIN, 参数说明: NULL
// 参数<3>: [返回文本型 SDT_BOOL], 参数说明: "结果返回文本型，默认返回字节集型。"
// 参数<4>: [&执行结果 SDT_BOOL], 参数说明: "如果提供本参数，其中将被写入本命令的执行结果——执行成功时为真，执行失败时为假。"
ICONV_EXTERN_C void Kiiconv_kiiconv_iconvEx_5_Kiiconv(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf)
{
	//获取字节集长度和数据指针
	size_t dwDataLen = 0;//待转换数据长度
	LPBYTE pData = 0;//待转换的数据
	DATA_TYPE dtDataType = pArgInf[1].m_dtDataType & ~DT_IS_VAR;//数据类型
	bool dtIsVar = (pArgInf[1].m_dtDataType & DT_IS_VAR) != 0;//是否为变量地址

	switch (dtDataType)
	{
	case SDT_BIN:
		//字节集数据
		if (dtIsVar) {
			pData = GetAryElementInf(*pArgInf[1].m_ppBin, (LPINT)&dwDataLen);//待转换数据指针
		}
		else {
			pData = GetAryElementInf(pArgInf[1].m_pBin, (LPINT)&dwDataLen);//待转换数据指针
		}

		if (dwDataLen == 0) {//数据长度为0，直接返回
			if (pArgInf[3].m_pBool) *pArgInf[3].m_pBool = false;
			return;
		}
		break;
	case SDT_TEXT:
		//文本数据
		if (dtIsVar) {
			pData = (LPBYTE)*pArgInf[1].m_ppText;
			dwDataLen = strlen(*pArgInf[1].m_ppText);
		}
		else {
			pData = (LPBYTE)pArgInf[1].m_pText;
			dwDataLen = strlen(pArgInf[1].m_pText);
		}
		break;
	default:
		if (pArgInf[3].m_pBool) *pArgInf[3].m_pBool = false;
		return;
	}

	iconv_t cd = (iconv_t)pArgInf[0].m_int;
	size_t dwBuffLen = dwDataLen * 4;//缓冲区长度
	LPBYTE pBuff;
	char** outbuf;
	if (pArgInf[2].m_pBool) {//返回文本型
		pBuff = (LPBYTE)ealloc(dwBuffLen);
		outbuf = (char**)(pBuff);
	}
	else {
		pBuff = (LPBYTE)ealloc(sizeof(INT) * 2 + dwBuffLen);
		outbuf = (char**)(pBuff + sizeof(INT) * 2);
	}
	size_t outbytesleft = dwBuffLen;

	//后面4个参数都会改变，当执行一次后，pData 会移动到 pData+dwDataLen 的位置，dwDataLen=0 ，pBuff 也会移动到新的位置，dwBuffLen 返回 pBuff 还剩下多少
	size_t ret = iconv(cd, (char**)&pData, &dwDataLen, (char**)&outbuf, &outbytesleft);
	if (ret == -1) {
		if (pArgInf[3].m_pBool) *pArgInf[3].m_pBool = false;
		efree(pBuff);
		return;
	}
	if (!pArgInf[2].m_pBool) {
		*(LPINT)pBuff = 1;
		*(LPINT)(pBuff + sizeof(INT)) = dwBuffLen - outbytesleft;
	}
	pRetData->m_dtDataType = pArgInf[2].m_pBool ? SDT_TEXT : SDT_BIN;
	pRetData->m_pBin = pBuff;
	if (pArgInf[3].m_pBool) *pArgInf[3].m_pBool = true;
}


// 调用格式: SDT_INT 编码转换_转换, 命令说明: "对指定数据进行编码转换，返回转换后的数据。对同一个“转换句柄”，可多次执行本命令，直到该句柄被关闭。如果执行失败，将返回 -1。"
// 参数<1>: icd SDT_INT, 参数说明: "必须是“编码转换_打开/iconv_open()”命令所返回的句柄。"
// 参数<2>: inbuf SDT_INT, 参数说明: "待转换的数据指针，iconv执行后会改变这个值，直到 inbuf 的值等于 inbuf+inbytesleft，且inbytesleft=0表示数据指针转换完成"。"
// 参数<3>: inbytesleft SDT_INT, 参数说明: "待转换的数据长度，iconv执行后会改变这个值，直到为 0，表示数据指针转换完成。"
// 参数<4>: outbuf SDT_INT, 参数说明: "转换后的数据指针，iconv执行后会改变这个值，直到 outbuf 的值等于 outbuf+outbytesleft，且outbytesleft=0表示缓冲区数据已经满了。"
// 参数<5>: outbytesleft SDT_INT, 参数说明: "缓冲区剩余长度，iconv执行后会改变这个值，直到为 0，表示缓冲区数据已经满了。"
ICONV_EXTERN_C void Kiiconv_kiiconv_iconv_8_Kiiconv(PMDATA_INF pRetData, INT nArgCount, PMDATA_INF pArgInf) {
	pRetData->m_int = iconv((iconv_t)pArgInf[0].m_int, (char**)pArgInf[1].m_int, (size_t*)pArgInf[2].m_int, (char**)pArgInf[3].m_int, (size_t*)pArgInf[4].m_int);
}